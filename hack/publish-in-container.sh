#!/bin/sh -x

set -o errexit
set -o nounset
set -o pipefail

cd "$(git rev-parse --show-toplevel)" || exit 0

C_DIR="/builds/$(basename $PWD)"
podman run --rm --network=host \
    -v "$PWD:$C_DIR" --workdir "$C_DIR" \
    alpine:edge \
      sh -c "
apk add --no-cache $(cat .alpine.pkgs) ;
git config --global --add safe.directory $C_DIR ;
./hack/publish.sh ${@:-}
"
