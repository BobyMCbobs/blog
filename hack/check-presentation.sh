#!/bin/sh
# NOTE: sh to run in CI

set -o errexit
set -o nounset
set -o pipefail

cd "$(git rev-parse --show-toplevel)"
DOMAIN="$(hugo config --format json | jq -r .params.plausible.domain)"

FAILURE=false

function _f {
    NAME="$1"
    CHECK="$2"
    if ! grep "$CHECK" -q < "$FILE"; then
        echo "FAILED($FILE): $NAME"
        FAILURE=true
    fi
}

for FILE in $(find ./static/presentations -mindepth 2 -name '*.html'); do
    echo "CHECKING: $FILE"
    _f 'has plausible' "<script defer data-domain=\"$DOMAIN\" src=\"https://plausible.io/js/script.js\"></script>"
    _f 'has back link' '<a class=back-nav href="/presentations">⏪ back</a'
done


if [ "$FAILURE" = true ]; then
    exit 1
fi
