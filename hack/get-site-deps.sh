#!/bin/bash

set -o errexit
set -o nounset
set -o pipefail

cd $(git rev-parse --show-toplevel)

OUTPUT_FOLDER=assets/js/ext

rm -rf "${OUTPUT_FOLDER}"
mkdir -p "${OUTPUT_FOLDER}"

SCRIPTS=(
    https://cdn.jsdelivr.net/npm/canvas-confetti@1.9.2/dist/confetti.browser.min.js
    https://cdn.jsdelivr.net/npm/dayjs@1.11.10/dayjs.min.js
    https://cdn.jsdelivr.net/npm/dayjs@1.11.10/plugin/timezone.js
    https://cdn.jsdelivr.net/npm/dayjs@1.11.10/plugin/utc.js
    https://cdn.jsdelivr.net/npm/dayjs@1.11.10/plugin/advancedFormat.js
    https://raw.githubusercontent.com/cure53/DOMPurify/3.1.6/dist/purify.min.js
)

cd "${OUTPUT_FOLDER}"
for SCRIPT in ${SCRIPTS[*]}; do
    curl -O -L -s "${SCRIPT}"
done
