#!/bin/bash

set -o errexit
set -o nounset
set -o pipefail

cd "$(git rev-parse --show-toplevel)" || exit 0

export APP_SERVE_FOLDER="${PWD}/public"
    APP_PORT=:8080 \
    BASE_URL="http://localhost:${APP_PORT//:}/"
hugo --baseURL="${BASE_URL}"
echo "URL: ${BASE_URL}"
go-http-server
