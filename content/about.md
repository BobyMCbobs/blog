---
title: "About me"
img: '/images/misc/caleb-woodbine.webp'
type: "page"
---

Hi there! :wave:

My name is Caleb and :round_pushpin: I live in Pōneke, Aotearoa (Wellington, New Zealand).

# :muscle: Skills

I love to build meaningful software -- it is my passion. I write in

{{< interestitems Go JavaScript VueJS HTML CSS Python PHP Bash Clojure >}}

I've been working for five years on and with cloud technologies. The tools that I'm most confident in are

{{< interestitems Kubernetes Containers "Docker / Podman" Terraform Cert-Manager nginx PowerDNS PostgreSQL Knative Serving Linux Helm Envoy Flux >}}

The future is [open and freely available](https://simple.wikipedia.org/wiki/Free_and_open-source_software); you can see all my contributions [_here_](https://github.com/BobyMCbobs) and [_here_](https://gitlab.com/BobyMCbobs).

I also like to make music, check out [my project isolaet](https://isolaet.com/).

# :globe_with_meridians: Some sites I've built

- [this site](/): home
- [isolaet.com](https://isolaet.com/): my music project
- [flattrack.io](https://flattrack.io/): collaboration software
- [chalkhouse.nz](https://chalkhouse.nz/): animation studio
- [absolutelycraycray.com](https://absolutelycraycray.com/): animation studio
- [liamfitzpatrick.art](https://liamfitzpatrick.art/): portfolio

# :thinking: Topics I'm interested in

{{< interestitems "Automation" "Cloud Native systems architecture" "Collaboration" "Container orchestration" "Convergent web applications" "Databases" "Edge computing" "Frontend applications" "GitOps" "Image-based operating systems" "Infrastructure sovereignty" "Linux desktop" "Microservice architecture" "Multi-cloud" "Optimised container builds" "Reproduceable infrastructure" "Reproduceable software builds" "Secure software supply chain" "Serverless" "Service proxies with Envoy" "Virtualisation" "WASM" >}}

# :computer: Projects

of current or past involvement

- Kubernetes ([kubernetes.io](https://kubernetes.io))
  - k8s-conformance testing
    - writing tests in Go, which are consumed by every Kubernetes distribution when summitting their products
  - k8s-conformance bot
    - for verifying the test results for each Kubernetes distribution
  - sig-k8s-infra
    - laying out the infrastructure used by the Kubernetes community
    - artifacts and registry cloud resource cost usage and traffic data graph aggregation 
- FlatTrack ([flattrack.io](https://flattrack.io))
  - Built for small to medium flatting situations
  - Go backend
  - VueJS frontend
  - Postgres database
  - Scalable
- Safe Surfer ([safesurfer.io](https://safesurfer.io))
  - Production grade PowerDNS on Kubernetes
  - OpenWRT-based Routers
  - Web services
  - Desktop app
  - Android app
  - Web servers [like go-http-server](https://gitlab.com/BobyMCbobs/go-http-server)
  - Complete infrastructure rebuild
- Pairing platforms
  - A Kubernetes native pairing environment [github.com/sharingio/pair](https://github.com/sharingio/pair); using cluster-api, PowerDNS as well as custom tooling and configuration to tie the stack together

# :eyes: In the wild

Presentations are [here](/presentations).

Other things include:
  - [CNCF 2024 community awards](https://www.cncf.io/announcements/2024/11/14/cloud-native-computing-foundation-announces-the-2024-community-awards-winners/)
  - [Knative blog: Knative Serving RuntimeClassNames](https://knative.dev/blog/articles/configurable-runtimeclassnames/)
  - [CNCF blog: scaling down a git repo a tidy up of CNCF k8s-conformance](https://www.cncf.io/blog/2024/01/12/scaling-down-a-git-repo-a-tidy-up-of-cncf-k8s-conformance/)
  - [CNCF blog: Kubernetes conformance updates for October 2022](https://www.cncf.io/blog/2022/10/19/kubernetes-conformance-updates-for-october-2022)
  - [Kubernetes blog: registry.k8s.io faster cheaper GA](https://kubernetes.io/blog/2022/11/28/registry-k8s-io-faster-cheaper-ga)
  - [Kubernetes blog: meet our contributors AU NZ EP02](https://kubernetes.io/blog/2022/03/16/meet-our-contributors-au-nz-ep-02/#caleb-woodbine-https-github-com-bobymcbobs)
  - [Kubernetes community awards 2022](https://www.kubernetes.dev/community/awards/2022/#k8s-infra)

# :email: Contact

Contact me at [calebwoodbine[dot]public[at]gmail[dot]com](mailto:) or on [Linkedin](/linkedin)

