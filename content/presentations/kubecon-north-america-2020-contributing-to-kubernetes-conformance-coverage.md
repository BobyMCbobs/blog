---
title: "KubeCon North America 2020: Contributing to Kubernetes Conformance Coverage"
date: "2020-11-17"
image: /images/2020/11/kccncna20.webp
---

watch on YouTube: https://www.youtube.com/watch?v=8iSXMewnIzg

conference link: https://events.linuxfoundation.org/archive/2020/kubecon-cloudnativecon-north-america/

