---
title: "KubeCon Europe 2021: Contributing to Kubernetes Conformance Coverage"
date: "2021-05-04"
image: /images/2021/05/kccnceu21.webp
---

watch on YouTube: https://www.youtube.com/watch?v=05NMwOhD6Ks

conference link: https://events.linuxfoundation.org/archive/2021/kubecon-cloudnativecon-europe/

