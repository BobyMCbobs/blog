---
title: "KubeCon North America 2021: Conformance Testing the Kubernetes API: Tooling that Makes the Difference"
date: "2021-10-11"
image: /images/2021/10/kccncna21.webp
---

watch on YouTube: https://www.youtube.com/watch?v=IQsBahak7PQ

conference link: https://events.linuxfoundation.org/archive/2021/kubecon-cloudnativecon-north-america/

