---
title: "Kubernetes Community Day Sydney 2023: My Journey into Kubernetes through client-go and conformance test writing"
date: "2023-08-22"
image: /images/2023/08/cover.webp
---

view presentation slides: [here](/presentations/my-journey-into-kubernetes-through-client-go-and-conformance-test-writing/)

conference link: https://community.cncf.io/events/details/cncf-kcd-australia-presents-kubernetes-community-day-australia-2023
