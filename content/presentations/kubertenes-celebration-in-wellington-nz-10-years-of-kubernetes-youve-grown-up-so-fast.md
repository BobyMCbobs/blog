---
title: |
  KuberTENes celebration in Wellington, NZ: 10 years of Kubernetes "You've grown up so fast!" 🥹
date: "2024-06-06"
image: /images/2024/06/kubertenes-slide.webp
---

view presentation slides: [here](/presentations/10-years-of-Kubernetes/)

watch on YouTube: https://www.youtube.com/watch?v=DAoRRuV9ypo

conference link: https://community.cncf.io/events/details/cncf-wellington-presents-kubertenes-birthday-bash-wellington-new-zealand/

