---
title: "Wellington OpenShift Meetup (Security Automation): Trusted Workloads on Kubernetes with Sigstore's policy-controller"
date: "2023-09-21"
image: /images/2023/09/wellington-openshift-meetup-security-automation-2023-09-21-cover.webp
---

view presentation slides: [here](/presentations/trusted-workloads-on-kubernetes-with-sigstores-policy-controller/)

meetup link: https://www.meetup.com/wellington-openshift-meetup/events/295769187
