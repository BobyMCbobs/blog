---
title: "Auckland Kubernetes Meetup: Secure Software Supply Chain with Sigstore"
date: "2025-02-26"
image: /images/2025/02/secure-software-supply-chain-slide.webp
---

view presentation slides: [here](/presentations/secure-softare-supply-chain-with-Sigstore/)

watch on YouTube: [here](https://youtu.be/bHGRAGRaTiM?t=3208)

conference link: https://www.meetup.com/auckland-kubernetes/events/305896235

