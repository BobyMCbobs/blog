---
title: "Cloud Native Summit 2023: Ease into productivity with Knative"
date: "2023-09-05"
image: /images/2023/09/cover.webp
---

view presentation slides: [here](/presentations/ease-into-productivity-with-Knative/)

watch on YouTube: [here](https://youtu.be/sahgSf4e41M)

conference link: https://www.cloudnativesummit.co/

