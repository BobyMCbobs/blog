---
title: "islive.xyz"
type: "page"
---

Publicly managed community infrastructure with friends.

More info coming soon.

See: https://gitlab.com/islive.xyz/infra
