+++
title = "Single Node Cluster Kubernetes On Baremetal With openSUSE Kubic"
date = "2020-01-04"
tags = ["Cloud", "Containers", "Kubernetes", "Linux", "OS"]
+++
#+HUGO_BASE_DIR: ../
#+HUGO_SECTION: ./
#+HUGO_WEIGHT: auto
#+HUGO_AUTO_SET_LASTMOD: t
#+html: <br/>
#+AUTHOR: Caleb Woodbine <calebwoodbine.public@gmail.com>
#+DATE: 10th of December 2019
#+DATE_CREATED: 2020-01-04
#+DATE_UPDATED: 2020-01-04

Wanna run [[https://kubernetes.io][Kubernetes]] at home? Here's an easy way to do that.

* What is [[https://kubic.opensuse.org/][openSUSE Kubic]]?
openSUSE Kubic is a community Linux distribution intended for running server side containers.

It's also a [[https://www.cncf.io/certification/software-conformance/][Kubernetes conformant distribution]].

* OS architecture
openSUSE Kubic is read-only transaction OS, similar to [[https://silverblue.fedoraproject.org/][Fedora Silverblue]] and [[https://getfedora.org/coreos/][Fedora CoreOS]].

This distribution supports atomic rollbacks and separates user-data and container data out to ~/var~, which is in it's own partition.

The supported container runtimes are [[https://cri-o.io/][cri-o]] and [[https://podman.io/][Podman]]. Docker is unsupported, but compatible with the commands of Podman.

* Installation
Let's install the OS.

Grub boot menu:
#+html: <img style='margin-left: auto; margin-right: auto;' alt='kubic-grub' src='/images/2020/01/kubic-grub.webp'>
#+html: <br/>

Choose 'kubeadm node' the option in the System Role part of the installer:
#+html: <img style='margin-left: auto; margin-right: auto;' alt='kubic-install-system-role' src='/images/2020/01/kubic-install-system-role.webp'>
#+html: <br/>

Begin the installation:
#+html: <img style='margin-left: auto; margin-right: auto;' alt='kubic-confirm-install' src='/images/2020/01/kubic-confirm-install.webp'>
#+html: <br/>

Login into the system:
#+html: <img style='margin-left: auto; margin-right: auto;' alt='kubic-booted' src='/images/2020/01/kubic-booted.webp'>
#+html: <br/>

* Initialize Kubernetes on the host
Ensure that the Pod network CIDR is set for the Flannel Container Network Interface (CNI).

#+BEGIN_SRC bash
kubeadm init --pod-network-cidr=10.244.0.0/16
#+END_SRC

Initialize Kubernetes on the node with kubeadm:
#+html: <img style='margin-left: auto; margin-right: auto;' alt='kubic-kubeadm-init' src='/images/2020/01/kubic-kubeadm-init.webp'>
#+html: <br/>

* DNS issues
If you had DNS issues on throughout the installation, you will need to unfortunately configure the network's default gateway. Go to ~/etc/sysconfig/network/routes~ and add the line:

#+BEGIN_SRC text
default 192.168.1.1 - enp0s25
#+END_SRC
(Replacing enp0s25 with the name of your interface, and replacing 192.168.1.1 with the default gateway of your network)

* Preliminary setup
Now that Kubernetes has been initialized on the node, you'll need to add the kubeconfig to your home folder so you can talk to the Kubernetes API.

#+BEGIN_SRC bash
mkdir -p $HOME/.kube
cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
chown $(id -u):$(id -g) $HOME/.kube/config
#+END_SRC

* Test that you can talk to the API
The following command will return status information if all is well

#+BEGIN_SRC bash
kubectl cluster-info
#+END_SRC

#+html: <img style='margin-left: auto; margin-right: auto;' alt='kubic-kubectl-cluster-info' src='/images/2020/01/kubic-kubectl-cluster-info.webp'>
#+html: <br/>

* Setting things up
Allow Pods to run on the master
The following command removes the limitation of running Pods on the master node.

#+BEGIN_SRC bash
kubectl taint nodes --all node-role.kubernetes.io/master-
#+END_SRC

* Install a Network Overlay
For this setup we'll be using the Flannel CNI. Flannel is a simple Container Network Interface which allows service to talk to each other. For a more advanced CNI which provides PodNetworkPolicies check out [[https://www.projectcalico.org/][Calico]].

#+BEGIN_SRC bash
kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml
#+END_SRC

* Verifying the setup
The following command will output all Pods in all namespaces.

#+BEGIN_SRC bash
kubectl get pods -A
#+END_SRC

#+html: <img style='margin-left: auto; margin-right: auto;' alt='kubic-kubectl-get-pods' src='/images/2020/01/kubic-kubectl-get-pods.webp'>
#+html: <br/>

* Final thoughts
I've found openSUSE Kubic to be one of the easiest methods for bringing up a Kubernetes cluster inside a VM or on real hardware that is also specifically designed for containers. I've been running it at home for almost a month and it seems to be generally stable (just make sure that you have at least 6GB of RAM for system stability).
