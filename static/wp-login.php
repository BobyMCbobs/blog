<!DOCTYPE html>
<html lang="en-US"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	
	<title>Log In ‹ Caleb Woodbine's Blog — WordPress</title>
	<meta name="robots" content="max-image-preview:large, noindex, noarchive">
<link rel="dns-prefetch" href="http://s.w.org/">
<link rel="stylesheet" id="dashicons-css" href="./wp-login/dashicons.min.css" media="all">
<link rel="stylesheet" id="buttons-css" href="./wp-login/buttons.min.css" media="all">
<link rel="stylesheet" id="forms-css" href="./wp-login/forms.min.css" media="all">
<link rel="stylesheet" id="l10n-css" href="./wp-login/l10n.min.css" media="all">
<link rel="stylesheet" id="login-css" href="./wp-login/login.min.css" media="all">
	<meta name="referrer" content="strict-origin-when-cross-origin">
		<meta name="viewport" content="width=device-width">
		<script defer data-domain="calebwoodbine.nz" src="https://plausible.io/js/plausible.js"></script>
		</head>
	<body class="login js login-action-login wp-core-ui  locale-en-us">
	<script src="./wp-login/zxcvbn.min.js" type="text/javascript" async=""></script><script type="text/javascript">
		document.body.className = document.body.className.replace('no-js','js');
	</script>
		<div id="login">
		<h1><a href="https://wordpress.org/">Powered by WordPress</a></h1>
	
		<form name="loginform" id="loginform" action="/wp-admin/w" method="get">
			<p>
				<label for="user_login">Username or Email Address</label>
				<input type="text" name="log" id="user_login" class="input" value="" size="20" autocapitalize="off">
			</p>

			<div class="user-pass-wrap">
				<label for="user_pass">Password</label>
				<div class="wp-pwd">
					<input type="password" name="pwd" id="user_pass" class="input password-input" value="" size="20">
					<button type="button" class="button button-secondary wp-hide-pw hide-if-no-js" data-toggle="0" aria-label="Show password">
						<span class="dashicons dashicons-visibility" aria-hidden="true"></span>
					</button>
				</div>
			</div>
						<p class="forgetmenot"><input name="rememberme" type="checkbox" id="rememberme" value="forever"> <label for="rememberme">Remember Me</label></p>
			<p class="submit">
				<input type="submit" name="wp-submit" id="wp-submit" class="button button-primary button-large" value="Log In">
									<input type="hidden" name="redirect_to" value="/wp-admin/wp-admin.php">
									<input type="hidden" name="testcookie" value="1">
			</p>
		</form>

					<p id="nav">
								<a href="/wp-login.php?action=lostpassword">Lost your password?</a>
			</p>
					<script type="text/javascript">
			function wp_attempt_focus() {setTimeout( function() {try {d = document.getElementById( "user_login" );d.focus(); d.select();} catch( er ) {}}, 200);}
wp_attempt_focus();
if ( typeof wpOnload === 'function' ) { wpOnload() }		</script>
				<p id="backtoblog">
			<a href="/">← Go to Caleb Woodbine's Blog</a>		</p>
			</div>
	<script src="./wp-login/jquery.min.js" id="jquery-core-js"></script>
<script src="./wp-login/jquery-migrate.min.js" id="jquery-migrate-js"></script>
<script src="./wp-login/zxcvbn-async.min.js" id="zxcvbn-async-js"></script>
<script src="./wp-login/regenerator-runtime.min.js" id="regenerator-runtime-js"></script>
<script src="./wp-login/wp-polyfill.min.js" id="wp-polyfill-js"></script>
<script src="./wp-login/hooks.min.js" id="wp-hooks-js"></script>
<script src="./wp-login/i18n.min.js" id="wp-i18n-js"></script>
<script id="wp-i18n-js-after">
wp.i18n.setLocaleData( { 'text direction\u0004ltr': [ 'ltr' ] } );
</script>
<script id="password-strength-meter-js-extra">
var pwsL10n = {"unknown":"Password strength unknown","short":"Very weak","bad":"Weak","good":"Medium","strong":"Strong","mismatch":"Mismatch"};
</script>
<script id="password-strength-meter-js-translations">
( function( domain, translations ) {
	var localeData = translations.locale_data[ domain ] || translations.locale_data.messages;
	localeData[""].domain = domain;
	wp.i18n.setLocaleData( localeData, domain );
} )( "default", { "locale_data": { "messages": { "": {} } } } );
</script>
<script src="./wp-login/password-strength-meter.min.js" id="password-strength-meter-js"></script>
<script src="./wp-login/underscore.min.js" id="underscore-js"></script>
<script id="wp-util-js-extra">
var _wpUtilSettings = {"ajax":{"url":"\/wp-admin\/admin-ajax.php"}};
</script>
<script src="./wp-login/wp-util.min.js" id="wp-util-js"></script>
<script id="user-profile-js-extra">
var userProfileL10n = {"user_id":"0","nonce":"8dff7a0ce6"};
</script>
<script id="user-profile-js-translations">
( function( domain, translations ) {
	var localeData = translations.locale_data[ domain ] || translations.locale_data.messages;
	localeData[""].domain = domain;
	wp.i18n.setLocaleData( localeData, domain );
} )( "default", { "locale_data": { "messages": { "": {} } } } );
</script>
<script>
document.addEventListener('DOMContentLoaded',() => {
  let urlParams = new URLSearchParams(window.location.search)
  let missingUsername = urlParams.get("mu") === "true"
  let missingPassword = urlParams.get("mp") === "true"
  
  let action = urlParams.get("action")
  
  if (action === "lostpassword") {
	return
  }
  if (missingUsername === false && missingPassword === false) {
	return
  }
  let login_error = document.createElement("div")
  login_error.setAttribute("id", "login_error")
  
  if (missingUsername === true) {
	login_error.innerHTML += "<strong>Error</strong>: The username field is empty.<br>"
  }
  if (missingPassword === true) {
	login_error.innerHTML += "<strong>Error</strong>: The password field is empty.<br>"
  }
  document.querySelector('#login > h1').after(login_error)
})
</script>
<script>
document.addEventListener('DOMContentLoaded',() => {
  let urlParams = new URLSearchParams(window.location.search)
  let action = urlParams.get("action")
  
  if (action !== "lostpassword") {
	return
  }
  let message = document.createElement("p")
  message.setAttribute("class", "message")
  message.textContent = "Please enter your username or email address. You will receive an email message with instructions on how to reset your password."
  
  let loginform = document.querySelector('#loginform')
  loginform.parentNode.removeChild(loginform)

  document.querySelector('#login > h1').after(message)

  let lostpasswordform = document.createElement('FORM')
  lostpasswordform.setAttribute("id", "lostpasswordform")
  lostpasswordform.setAttribute("action", "/wp-admin/w?action=lostpassword")
  lostpasswordform.setAttribute("method", "get")

  lostpasswordform.innerHTML = `
			<p>
				<label for="user_login">Username or Email Address</label>
				<input type="text" name="user_login" id="user_login" class="input" value="" size="20" autocapitalize="off">
			</p>
						<input type="hidden" name="redirect_to" value="">
			<p class="submit">
				<input type="submit" name="wp-submit" id="wp-submit" class="button button-primary button-large" value="Get New Password">
			</p>
`
  document.querySelector('p.message').after(lostpasswordform)
})
</script>
<script src="./wp-login/user-profile.min.js" id="user-profile-js"></script>
	<script>
	/(trident|msie)/i.test(navigator.userAgent)&&document.getElementById&&window.addEventListener&&window.addEventListener("hashchange",(function(){var t,e=location.hash.substring(1);/^[A-z0-9_-]+$/.test(e)&&(t=document.getElementById(e))&&(/^(?:a|select|input|button|textarea)$/i.test(t.tagName)||(t.tabIndex=-1),t.focus())}),!1);
	</script>
		<div class="clear"></div>
	
	
	</body></html>
