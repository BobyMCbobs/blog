FROM alpine:edge AS build
RUN apk add --no-cache hugo tzdata
WORKDIR /site
COPY . /site
RUN hugo --gc --minify

FROM registry.gitlab.com/bobymcbobs/go-http-server:1.11.0
ENV APP_SERVE_FOLDER=/site
COPY --from=build /site/public /site
