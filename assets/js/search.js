(() => {
  document.addEventListener('DOMContentLoaded', () => {
    if (document.querySelector('article.search') === null) {
      return
    }

    const postsPath = "/index.xml"
    let posts = null

    const httpGet = (url) => {
      var xmlHttp = new XMLHttpRequest()
      xmlHttp.open("GET", url, false)
      xmlHttp.send(null)
      return xmlHttp.responseText
    }

    const domFromIndexXml = () => {
      const indexXmlContent = httpGet(postsPath)
      const parser = new DOMParser()
      const xmlDoc = parser.parseFromString(indexXmlContent, "text/xml")
      return xmlDoc
    }

    // https://stackoverflow.com/questions/1773550/convert-xml-to-json-and-back-using-javascript
    const xml2json = (xml) => {
      try {
        var obj = {};
        if (xml.children.length > 0) {
          for (var i = 0; i < xml.children.length; i++) {
            var item = xml.children.item(i);
            var nodeName = item.nodeName;

            if (typeof (obj[nodeName]) == "undefined") {
              obj[nodeName] = xml2json(item);
            } else {
              if (typeof (obj[nodeName].push) == "undefined") {
                var old = obj[nodeName];

                obj[nodeName] = [];
                obj[nodeName].push(old);
              }
              obj[nodeName].push(xml2json(item));
            }
          }
        } else {
          obj = xml.textContent;
        }
        return obj;
      } catch (e) {
        console.log(e.message);
      }
    }

    const loadIndexXmlPosts = () => {
      const dom = domFromIndexXml()
      const jsonFromDom = xml2json(dom)
      posts = jsonFromDom.rss.channel.item
      return posts
    }

    const getPostsFromSearch = (query = '') => {
      if (query === null) {
        query = ''
      }
      query = query.toLowerCase().split(' ').filter(i => i != '')
      if (posts === null) {
        loadIndexXmlPosts()
      }
      if (typeof posts.length === "undefined") {
        posts = [posts]
      }
      return posts.filter(i => {
        return query.every(e => {
          return i.title.toLowerCase().includes(e) || i.description.toLowerCase().includes(e)
        })
      })
    }

    const renderPostListItems = (query) => {
      const posts = getPostsFromSearch(query)
      const msg = document.querySelector("#msg")
      if (posts.length === 0) {
        msg.textContent = 'No results found.'
      } else {
        msg.textContent = ''
      }
      const template = document.querySelector("template#post-list-item")
      const postsList = document.querySelector("ul.posts-list")
      postsList.innerHTML = ''
      console.log({ query, posts })
      posts.forEach(n => {
        let clone = template.content.firstElementChild.cloneNode(true)
        let link = clone.querySelector("li a")
        link.textContent = `📄 ${n.title}`
        link.href = n.link
        postsList.append(clone)
      })
    }

    const searchInput = document.getElementById('searchInput')
    searchInput.onkeyup = () => renderPostListItems(searchInput.value)
  })
})()
