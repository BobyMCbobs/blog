(() => {
  document.addEventListener('DOMContentLoaded', () => {
    Array.prototype.slice.call(
      document.getElementsByTagName("html")
              .item(0)
              .getElementsByTagName("a")
    ).forEach(i =>
      i.hostname != window.location.hostname ?
        i.target = "_blank" :
        null)
  })
})()
