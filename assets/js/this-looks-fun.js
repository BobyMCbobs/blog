(() => {
  document.addEventListener('DOMContentLoaded',() => {
    let avatar = document.getElementById("app-header-avatar")
    if (avatar === null) {
      return
    }

    let confettiCanvas = document.createElement("canvas");
    confettiCanvas.setAttribute("class", "confetti-canvas")
    confettiCanvas.width = 320
    confettiCanvas.height = 270
    let appheader = document.getElementsByClassName("background-shapes")[0]
    appheader.appendChild(confettiCanvas);

    let aFunThing = confetti.create(confettiCanvas, {
      resize: true,
      useWorker: true
    })

    avatar.addEventListener("click", () => {
      aFunThing({
        particleCount: 200,
        spread: 80,
        origin: { y: 0.5, x: 0.5 },
        zIndex: -1
      })
    })
  })
})()
