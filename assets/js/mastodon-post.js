(() => {
  document.addEventListener('DOMContentLoaded', () => {
    if (document.querySelector('main div.home-page-hero') === null) {
      return
    }

    const postsURL = "https://mastodon.nz/@calebwoodbine.rss"
    let posts = null

    const httpGet = (url) => {
      var xmlHttp = new XMLHttpRequest()
      xmlHttp.open("GET", url, false)
      xmlHttp.send(null)
      return xmlHttp.responseText
    }

    const domFromIndexXml = () => {
      const indexXmlContent = httpGet(postsURL)
      const parser = new DOMParser()
      const xmlDoc = parser.parseFromString(indexXmlContent, "text/xml")
      return xmlDoc
    }

    // https://stackoverflow.com/questions/1773550/convert-xml-to-json-and-back-using-javascript
    const xml2json = (xml) => {
      try {
        var obj = {};
        if (xml.children.length > 0) {
          for (var i = 0; i < xml.children.length; i++) {
            var item = xml.children.item(i);
            var nodeName = item.nodeName;

            if (typeof (obj[nodeName]) == "undefined") {
              obj[nodeName] = xml2json(item);
            } else {
              if (typeof (obj[nodeName].push) == "undefined") {
                var old = obj[nodeName];

                obj[nodeName] = [];
                obj[nodeName].push(old);
              }
              obj[nodeName].push(xml2json(item));
            }
          }
        } else {
          obj = xml.textContent;
        }
        return obj;
      } catch (e) {
        console.log(e.message);
      }
    }

    const loadIndexXmlPosts = () => {
      const dom = domFromIndexXml()
      const jsonFromDom = xml2json(dom)
      posts = jsonFromDom.rss.channel.item
    }

    const renderPostItem = (post) => {
      const latestMastodonPost = document.querySelector('div.latest-mastodon-post')
      const postContent = document.querySelector("div.latest-mastodon-post a.posts-list-item-content span")
      if (post === null) {
        latestMastodonPost.innerHTML = ''
      }
      const cleanPostContent = DOMPurify.sanitize(post.description, { ALLOWED_TAGS: ['p', 'a', 'br'] })
      postContent.innerHTML = cleanPostContent
      Array.prototype.slice.call(
        postContent.getElementsByTagName("a")
      ).forEach(i =>
        i.hostname != window.location.hostname ?
          i.target = "_blank" :
          null)

      const postLink = document.querySelector("div.latest-mastodon-post a.posts-list-item-content")
      postLink.href = post.link

      const postDate = document.querySelector("div.latest-mastodon-post p.date")
      const formattedDate = dayjs(post.pubDate).format('MMM DD, YYYY, HH:mm')
      postDate.textContent = formattedDate
      latestMastodonPost.classList.remove('hidden')
    }

    loadIndexXmlPosts()
    renderPostItem(posts[0])
  })
})()
