(() => {
  let audioSource = document.createElement("SOURCE")
  audioSource.setAttribute("src", "/misc/2022-03-13 loop for site.mp3")
  audioSource.setAttribute("type", "audio/mpeg")

  let newAudio = document.createElement("AUDIO")
  newAudio.appendChild(audioSource)
  newAudio.setAttribute("id", "isolate-audio")
  document.addEventListener('DOMContentLoaded',() => {
    document.body.appendChild(newAudio)

    let audio = document.getElementById("isolate-audio")
    audio.volume = 0
    audio.loop = true
    audio.load()
    let maxVolume = 0.1
    let playAudio = () => {
      audio.play()
    }
    let fadeInAudioVolume = () => {
      setInterval(() => {
        if (audio.volume === maxVolume) {
          return
        }
        if (audio.currentTime > audio.duration - 5) {
          return
        }
        audio.volume += 0.05
      }, 500)
    }
    let fadeOutAudioVolume = () => {
      setInterval(() => {
        if (audio.volume !== maxVolume) {
          return
        }
        if (audio.currentTime < audio.duration - 5) {
          return
        }
        audio.volume -= 0.05
      }, 500)
    }
    const isolaetLink = document.getElementById("footer-social-Isolaet")
    isolaetLink.addEventListener("mouseover", () => {
      playAudio()
      fadeInAudioVolume()
    })
    isolaetLink.addEventListener("mouseout", () => {
      fadeOutAudioVolume()
      audio.pause()
      audio.currentTime = 0
      audio.volume = 0
    })
  })
})()
