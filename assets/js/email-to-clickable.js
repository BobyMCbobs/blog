(() => {
  if (window.location.pathname.startsWith('/about') === false) {
    return
  }
  document.addEventListener('DOMContentLoaded', () => {
    const contactLink = document.querySelector('.post-content > p:last-child').children[0]
    contactLink.textContent = contactLink.textContent.replaceAll('[dot]', '.').replace('[at]', '@')
    contactLink.href = `mailto:${contactLink.textContent}`
  })
})()
