(() => {
  const tz = "Pacific/Auckland"
  const format = "🇳🇿🥝🕰️  HH:mm, MMMM Do YYYY"
  const sleepms = 1000

  document.addEventListener('DOMContentLoaded',() => {
    dayjs.extend(window.dayjs_plugin_utc)
    dayjs.extend(window.dayjs_plugin_timezone)
    dayjs.extend(window.dayjs_plugin_advancedFormat)

    dayjs.tz.setDefault(tz)

    function setLocalTime() {
      let localnztime = document.getElementsByClassName("localnztime")[0]
      let timenow = dayjs(new Date()).tz(tz).format(format)
      localnztime.innerText = timenow
      setTimeout(setLocalTime, sleepms)
    }

    setLocalTime()
  })
})()
